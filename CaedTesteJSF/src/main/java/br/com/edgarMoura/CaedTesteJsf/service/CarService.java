package br.com.edgarMoura.CaedTesteJsf.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.edgarMoura.CaedTesteJsf.dao.CarDao;
import br.com.edgarMoura.CaedTesteJsf.exception.DuplicateRecordException;
import br.com.edgarMoura.CaedTesteJsf.model.Car;


@Named
@ApplicationScoped
public class CarService {

	@Inject
	private CarDao carDao;
	
	
	public List<Car> findCar() throws Exception{
		try {
			List<Car> cars = carDao.findCars();
			return cars;
		}catch (Exception e) {
			throw new Exception();
		}
	}
	
	public boolean validateCarAlreadyRegistered(Car car) {
		Car c = carDao.findCarAlreadyRegistered(car);
		return c != null ? true : false; 		
	}
	
	public Car findById(Long id) {
		Car c = carDao.findById(id);
		return c != null ? c : null;
	}
	
	public Car save(Car car) throws Exception {
		try {
			Car c = null;
			if (car.getId() == null && validateCarAlreadyRegistered(car)) {
				throw new DuplicateRecordException("Dados j� cadastrados");
			}
			c = carDao.save(car);
			return c;
		} catch (Exception e) {
			if (!(e instanceof DuplicateRecordException)) {
				carDao.roolback();
				throw new Exception(e);
			} else {
				throw new DuplicateRecordException("Dados j� cadastrado");
			}
		}
	}
	
	public void deleteCar(Car car) throws Exception  {
		try {
			
			carDao.remove(car);
			
		}catch (Exception e ) {
			throw new Exception();
		}
	}
	

	
}
