package br.com.edgarMoura.CaedTesteJsf.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.edgarMoura.CaedTesteJsf.exception.DuplicateRecordException;
import br.com.edgarMoura.CaedTesteJsf.model.Car;
import br.com.edgarMoura.CaedTesteJsf.service.CarService;
import br.com.edgarMoura.CaedTesteJsf.util.FacesUtil;

@ManagedBean(name = "carManagementController")
@ViewScoped
public class CarManagementController {

private static final Logger logger = Logger.getLogger(CarManagementController.class);
	
	private static final String ID_CAR = "idCar";

	public Car objRegister = new Car();
	private Long idCar;
	private boolean savedRecord = false; 
	
	@Inject
	private CarService service;
	
	@PostConstruct
	private void init() {
		try {
			idCar = FacesUtil.getLongParameter(ID_CAR);
			objRegister = service.findById(idCar);
		} catch (Exception e){
			logger.error(e.getMessage(), e);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao carregar a p�gina.", ""));
		}

	}
	
	public void edit() {
		try {
			if (validatefieldfill()) {
			
				service.save(objRegister);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso", ""));
				this.setSavedRecord(true);
				
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Campos obrigat�rios n�o foram preenchidos", ""));
			}
		} catch (Exception e) {
			if (e instanceof DuplicateRecordException) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
			} else {
				logger.error(e.getMessage(), e);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Ocorreu um erro de processamento ao salvar", ""));
			}
		}

	}
	
	public void delete() {
		try {
				service.deleteCar(objRegister);
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro removido com sucesso", ""));
				objRegister = new Car();
				this.setSavedRecord(true);
				
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro ao remover o registro", ""));
		}
	}
	
	private boolean validatefieldfill() {
		if (objRegister.getName().isEmpty() || objRegister.getColor().isEmpty()
				|| objRegister.getYear() == null) {
			return false;
		} else {
			return true;
		}
	}
	public Car getObjRegister() {
		return objRegister;
	}

	public void setObjRegister(Car objRegister) {
		this.objRegister = objRegister;
	}

	public Long getIdCar() {
		return idCar;
	}


	public void setIdCar(Long idCar) {
		this.idCar = idCar;
	}

	public boolean isSavedRecord() {
		return savedRecord;
	}

	public void setSavedRecord(boolean savedRecord) {
		this.savedRecord = savedRecord;
	}

}
