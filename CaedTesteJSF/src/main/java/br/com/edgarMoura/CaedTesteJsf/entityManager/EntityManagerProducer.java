package br.com.edgarMoura.CaedTesteJsf.entityManager;

import java.util.Locale;
import java.util.Locale.Category;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@ApplicationScoped
public class EntityManagerProducer {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("spring_init");

	public EntityManagerProducer() {
		Locale.setDefault(Locale.getDefault(Category.FORMAT));
	}

	@ApplicationScoped
	@Produces
	public EntityManager createEntityManager() {
		return factory.createEntityManager();
	}

	public void close(@Disposes EntityManager manager) {
		manager.close();
	}

}
