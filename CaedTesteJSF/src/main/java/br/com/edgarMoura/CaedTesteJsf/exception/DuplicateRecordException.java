package br.com.edgarMoura.CaedTesteJsf.exception;

public class DuplicateRecordException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public DuplicateRecordException(String erro) {
		super(erro);
	}
	

}
