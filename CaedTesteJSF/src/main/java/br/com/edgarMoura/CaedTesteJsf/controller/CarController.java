package br.com.edgarMoura.CaedTesteJsf.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.edgarMoura.CaedTesteJsf.model.Car;
import br.com.edgarMoura.CaedTesteJsf.service.CarService;


@ManagedBean(name = "carController")
@ViewScoped
public class CarController {

	private static final Logger logger = Logger.getLogger(CarController.class);
	
	List<Car> cars = new ArrayList<>();
	Car car = new Car();
	
	@Inject
	private CarService service;
	
	@PostConstruct
	private void init() {
		try {
			cars = service.findCar();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ocorreu um erro no carregamento da p�gina", ""));
		}
	}
	
	
	
	public List<Car> getCars() {
		return cars;
	}
	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
		
}
