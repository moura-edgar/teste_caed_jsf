package br.com.edgarMoura.CaedTesteJsf.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import br.com.edgarMoura.CaedTesteJsf.entityManager.GenericDao;
import br.com.edgarMoura.CaedTesteJsf.model.Car;


@Named
@ApplicationScoped
public class CarDao extends GenericDao<Car> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	public List<Car> findCars()  {
		StringBuilder sql = new StringBuilder();
		sql.append("select c from Car c");
		List<Car> resultList = getManager().createQuery(sql.toString(), Car.class).getResultList();
		return resultList;
	}
	
	public Car findById(Long id) {
		Car car = getManager().find(Car.class, id);
		return car == null ? null : car;
	}


	public void remove(Car car) {
		if(!getManager().getTransaction().isActive()) {
			getManager().getTransaction().begin();
		}
		getManager().remove(car);
		getManager().getTransaction().commit();
	}


	public void update(Car e) {
		getManager().merge(e);
		
	}
	
	public Car findCarAlreadyRegistered(Car car) {
		StringBuilder sql = new StringBuilder();
		sql.append("select c from Car c where 1=1 ");
		sql.append(" and c.color = ").append("'").append(car.getColor()).append("'");
		sql.append(" and c.name = ").append("'").append(car.getName()).append("'");
		sql.append(" and c.year = ").append(car.getYear());
		
		List<Car> cars = getManager().createQuery(sql.toString(), Car.class).getResultList();
		
		return cars.isEmpty() ? null : cars.get(0); 
		
	}

}
