package br.com.edgarMoura.CaedTesteJsf.entityManager;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Locale.Category;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class GenericDao<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager manager;

	public GenericDao() {
		Locale.setDefault(Locale.getDefault(Category.FORMAT));
	}

	public void beginTransaction() {
		if (!manager.getTransaction().isActive()) {
			manager.getTransaction().begin();
		}
	}

	public void roolback() {
		manager.getTransaction().rollback();
	}

	public T findById(Class<T> entityClass, Long id) throws Exception {
		return manager.find(entityClass, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll(Class<T> entityClass) throws Exception {
		StringBuilder sql = new StringBuilder("select e from ");
		sql.append(entityClass.getSimpleName()).append(" e");
		List<T> resultList = manager.createQuery(sql.toString()).getResultList();
		return resultList;
	}

	public Long getNextId(Class<T> entityClass) throws Exception {
		StringBuilder sql = new StringBuilder(" select max (id) from ").append(entityClass.getSimpleName());
		Object retorno = getManager().createQuery(sql.toString()).getSingleResult();
		Long id = new Long(0);
		if (retorno != null)
			id = new Long(retorno.toString());
		return ++id;
	}

	public T save(T t) throws Exception {
		if (!manager.getTransaction().isActive()) {
			manager.getTransaction().begin();
		}
		T obj = manager.merge(t);
		manager.flush();
		manager.getTransaction().commit();
		return obj;
	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

}
