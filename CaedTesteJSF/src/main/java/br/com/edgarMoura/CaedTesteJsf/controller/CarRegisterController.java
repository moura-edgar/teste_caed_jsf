package br.com.edgarMoura.CaedTesteJsf.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import br.com.edgarMoura.CaedTesteJsf.exception.DuplicateRecordException;
import br.com.edgarMoura.CaedTesteJsf.model.Car;
import br.com.edgarMoura.CaedTesteJsf.service.CarService;

@ManagedBean(name = "carRegisterController")
@ViewScoped
public class CarRegisterController {
	
	private static final Logger logger = Logger.getLogger(CarRegisterController.class);
	
	public Car objRegister = new Car();
	private boolean savedRecord = false; 
	
	@Inject
	private CarService service;
	
	
	public void save() {
		try {
			if (validatefieldfill()) {
			
				service.save(objRegister);
				objRegister = new Car();
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro salvo com sucesso", ""));
				this.setSavedRecord(true);
				
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Campos obrigat�rios n�o foram preenchidos", ""));
			}
		} catch (Exception e) {
			if (e instanceof DuplicateRecordException) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), ""));
			} else {
				logger.error(e.getMessage(), e);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Ocorreu um erro de processamento ao salvar", ""));
			}
		}

	}
	
	private boolean validatefieldfill() {
		if (objRegister.getName().isEmpty() || objRegister.getColor().isEmpty()
				|| objRegister.getYear() == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public Car getObjRegister() {
		return objRegister;
	}

	public void setObjRegister(Car objRegister) {
		this.objRegister = objRegister;
	}

	public boolean isSavedRecord() {
		return savedRecord;
	}

	public void setSavedRecord(boolean savedRecord) {
		this.savedRecord = savedRecord;
	}

}
