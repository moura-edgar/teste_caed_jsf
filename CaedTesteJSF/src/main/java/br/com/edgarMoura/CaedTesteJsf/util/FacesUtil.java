package br.com.edgarMoura.CaedTesteJsf.util;

import javax.faces.context.FacesContext;

public class FacesUtil {
	
	public static String getStringParameter(String param) {
		return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(param);
	}
	
	public static Long getLongParameter(String param) {
		return Long.valueOf(getStringParameter(param));
	}

}
